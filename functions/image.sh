die(){ printf '%s\n' "$@" >&2; exit 1; }

_cb_compress(){
local file="${1}"
# Apply compression
if [ ! -z "${CB_IMAGE_COMPRESS}" ]
then
	case "${CB_IMAGE_COMPRESS}" in
	gzip)
		gzip "${file}"
		;;
	xz)
		xz "${file}"
		;;
	*)
		Echo_warning "Compression type not supported. File a request."
		;;
	esac
fi
}

_cb_checksum(){
local file="${1}"
# Apply checksum
if [ ! -z "${CB_IMAGE_CHKSUM}" ]
then
	case "${CB_IMAGE_CHKSUM}" in
	md5)
		md5sum "${file}" > "${file}".md5
		;;
	*)
		Echo_warning "Checksum type not supported. File a request."
		;;
	esac
fi
}

_cb_zsync(){
local file="${1}"
# Apply checksum
if [ ! -z "${CB_IMAGE_ZSYNC}" ]
then
	case "${CB_IMAGE_ZSYNC}" in
	zsync)
		zsyncmake -C -b 2048 -u ${file} ${file}
		;;
	*)
		Echo_warning "Checksum type not supported. File a request."
		;;
	esac
fi
}

_cb_virt(){
local file="${1}"
# Apply checksum
if [ ! -z "${CB_IMAGE_VIRT}" ]
then
	case "${CB_IMAGE_VIRT}" in
	qemu)
		qemu-img convert -O vdi ${file} ${file%.*}.vdi
		chmod a+r ${file%.*}.vdi
		;;
	vbox)
		echo "vbox images not yet supported."
		;;
	*)
		Echo_warning "Checksum type not supported. File a request."
		;;
	esac
fi
}

