#!/bin/sh

## image-builder - Collabora System Build Scripts
## Copyright (C) 2012 Hector Oron <hector.oron@collabora.co.uk>
##
## image-builder comes with ABSOLUTELY NO WARRANTY; for details see COPYING.
## This is free software, and you are welcome to redistribute it
## under certain conditions; see COPYING for details.

# Including common functions
. "${LIVE_BUILD:-/usr/lib/live/}"/build.sh
. "${CB_BASE:-/usr/lib/image/}"/scripts/build.sh

# Setting static variables
DESCRIPTION="$(Echo 'Collabora hwpack build system')"
HELP=""
USAGE="${PROGRAM} [--force]"

# Reading hwpack configuration file
Read_conffiles config/image-builder.cfg

if [ "${CB_RELEASE_INTERNAL}" = "yes" ] ; then
	pdir=internal
else
	pdir=public
fi

LB_HWPACK_EXTRAOPTS="--debug"

# Build hwpack
for CB_HWPACK_CONFIG in ${CB_HWPACK_CONFIGS}
do
	if [ -z ${CB_HWPACK_CONFIG} ]
	then
		echo "W: Skipping hwpack build. No config file found."
	else
		CB_HWPACK_BASENAME=${CB_HWPACK_CONFIG%.yaml}
		_arch=$(echo ${CB_HWPACK_CONFIG} | cut -d- -f2)
		if [ -f "${CB_BUILDSHAREDDIR:-.}"/${pdir}/hwpack-build/hwpack_${CB_HWPACK_BASENAME}_${CB_BUILDSTAMP}_"${_arch}"_supported.tar.gz ] && \
		[ x"${CB_FORCE_REBUILD}" != "xyes" ]
		then
			echo "I: hwpack_${CB_HWPACK_BASENAME}_${CB_BUILDSTAMP}_${_arch}_supported.tar.gz already built."
			echo "   Reusing previous hwpack build. A rebuild can be forced setting force-rebuild flag."
		else
			echo "I: Building hwpack from file at ${CB_BUILDDIR:-.}/config/hwpack/${CB_HWPACK_CONFIG}"
			mkdir -p ${CB_BUILDDIR:-.}/config/hwpack/
			if [ x"${CB_RELEASE_INTERNAL}" = "xyes" ]
			then
				cp -rfa ${CB_BASE}/customization/hwpack-internal/* ${CB_BUILDDIR:-.}/config/hwpack
			else
				cp -rfa ${CB_BASE}/customization/hwpack/* ${CB_BUILDDIR:-.}/config/hwpack
			fi
			mkdir -p ${CB_BUILDDIR:-.}/hwpack-build/${CB_BUILDSTAMP}/ ; cd ${CB_BUILDDIR:-.}/hwpack-build/${CB_BUILDSTAMP}/ ;
			TMPDIR="${CB_BUILDDIR:-.}"/hwpack-build/ \
			linaro-hwpack-create "${LB_HWPACK_EXTRAOPTS}" ${CB_BUILDDIR:-.}/config/hwpack/"${CB_HWPACK_CONFIG}" ${CB_BUILDSTAMP} \
			 || echo "E: $0: Failed to build hwpack ${CB_BUILDDIR:-.}/config/hwpack/${CB_HWPACK_CONFIG}"
			mv ${CB_BUILDDIR:-.}/hwpack-build/${CB_BUILDSTAMP}/hwpack_${CB_HWPACK_BASENAME}_${CB_BUILDSTAMP}_${_arch}_supported.* \
				 "${CB_BUILDSHAREDDIR:-.}"/${pdir}/hwpack-build/
			cd ${CB_BUILDDIR:-.}/
		fi
	fi
done

