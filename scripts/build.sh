#!/bin/sh

CB_BASE="${CB_BASE:-/usr/share/image/utils/}"
export CB_BASE

# Source global functions
for FUNCTION in "${CB_BASE}"/functions/*.sh
do
	. "${FUNCTION}"
done

