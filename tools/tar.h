#ifndef __TAR_H__
#define __TAR_H__

struct tar_header {
	char file_name[100];
	char file_mode[8];
	char owner_id[8];
	char group_id[8];
	char file_size[12];
	char mtime[12];
	char csum[8];
	char type[1];
	char linkname[100];
/* ustar only */
	char ustar[6];
	char ustar_ver[2];
	char owner_name[32];
	char group_name[32];
	char dev_major[8];
	char dev_minor[8];
	char file_name_prefix[155];
	char pad[12];
};

#endif /* __TAR_H__ */
