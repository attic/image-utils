#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <lzma.h>
#include <unistd.h>
#include "tar.h"

struct tar_state {
	bool in_file;
	struct tar_header ch;
	int bytes_left;
	int bytes_archived;
	int unpadded_bytes_left;
};

struct splitter_state {
	uint32_t preset;
	uint8_t inbuf[BUFSIZ];
	uint8_t outbuf[BUFSIZ];
	int max_file_size;
	int current_file_size;
	int submit;
	int fno;
	bool verbose;
	bool extreme;
	bool parse_tar;
	char template[1024];
	FILE *infile;
	FILE *outfile;
	struct tar_state ts;
};

static void usage(void)
{
	fprintf(stderr, "image_splitter\n");
	fprintf(stderr, "compress an archive into multiple files of similar size\n");
	fprintf(stderr, "Options:\n");
	fprintf(stderr, " -p # : preset level for xz compression\n");
	fprintf(stderr, " -e   : Use extreme xz compression\n");
	fprintf(stderr, " -t   : enable parsing of tar files\n");
	fprintf(stderr, " -s # : File size (in Megabytes)\n");
	fprintf(stderr, " -b $ : output base filename (default: output)\n");
	fprintf(stderr, " -d # : number of digits in output filenames\n");
	fprintf(stderr, " -f $ : input file (default: stdin)\n");
	fprintf(stderr, " -v   : Verbose output\n");
}

static bool init_encoder(lzma_stream *strm, uint32_t preset)
{
	if (lzma_easy_encoder(strm, preset, LZMA_CHECK_CRC64) == LZMA_OK)
		return true;

	fprintf(stderr, "Failed to initialize encoder\n");
	return false;
}

static bool write_out(struct splitter_state *state, lzma_stream *strm)
{
	if (strm->avail_out == 0) {
		size_t write_size = sizeof(state->outbuf);

		if (fwrite(state->outbuf, 1, write_size, state->outfile)
				!= write_size) {
			fprintf(stderr, "Write error: %s\n",
					strerror(errno));
			return false;
		}

		// Reset next_out and avail_out.
		strm->next_out = state->outbuf;
		strm->avail_out = sizeof(state->outbuf);
		state->current_file_size += write_size;
	}
	return true;
}

static int flush(struct splitter_state *state, lzma_stream *strm, lzma_action mode)
{
	lzma_ret ret;
	do {
		ret = lzma_code(strm, mode);
		if (strm->avail_out == 0 || ret == LZMA_STREAM_END) {
			size_t write_size = sizeof(state->outbuf) - strm->avail_out;

			if (fwrite(state->outbuf, 1, write_size, state->outfile)
					!= write_size) {
				fprintf(stderr, "Write error: %s\n",
						strerror(errno));
				return false;
			}

			strm->next_out = state->outbuf;
			strm->avail_out = sizeof(state->outbuf);
			state->current_file_size += write_size;
		}
	} while (ret == LZMA_OK);

	if (ret == LZMA_STREAM_END)
		return true;

	return false;
}

static int parseoctal(char *str)
{
	int out = 0, i;
	for (i = 0; i < 11; i++)
		out = out*8 + str[i] -'0';
	return out;
}

static bool get_input(struct splitter_state *state, lzma_stream *strm)
{
	int to_read;
	if (state->ts.bytes_left > sizeof(state->inbuf))
		to_read = sizeof(state->inbuf);
	else
		to_read = state->ts.bytes_left;
	strm->next_in = state->inbuf;
	if (!to_read) {
		strm->avail_in = fread(state->inbuf, 1, sizeof(struct tar_header),
				state->infile);
		state->submit += strm->avail_in;
		memcpy(&state->ts.ch, state->inbuf, sizeof(struct tar_header));
		state->ts.in_file = true;
		state->ts.bytes_archived = 0;
		state->ts.bytes_left = parseoctal(state->ts.ch.file_size);
		state->ts.unpadded_bytes_left = state->ts.bytes_left;
		if (state->ts.bytes_left % 512)
			state->ts.bytes_left += 512 - (state->ts.bytes_left % 512);
	} else {
		strm->avail_in = fread(state->inbuf, 1, to_read,
				state->infile);
		state->submit += strm->avail_in;
		state->ts.bytes_left -= to_read;
		state->ts.bytes_archived += to_read;
		state->ts.unpadded_bytes_left -= to_read; // this could end up negative
		if (ferror(state->infile)) {
			fprintf(stderr, "Read error: %s\n",
					strerror(errno));
			return false;
		}
	}
	return true;
}

static void set_cksum(struct tar_header *th)
{
	int i, csum = 0;
	unsigned char *ptr = (unsigned char *)th;
	memset(th->csum, ' ', 8);
	for (i = 0; i < sizeof(struct tar_header); i++)
		csum += *ptr++;
	snprintf(th->csum, 7, "%.6o", csum);
	th->csum[6] = 0;
	th->csum[7] = ' ';
}

static bool repeat_header(struct splitter_state *state, lzma_stream *strm)
{
	if (state->ts.bytes_left == 0) {
		return true;
	}
	if (state->ts.unpadded_bytes_left < 1) {
		return false;
	}
	state->ts.ch.type[0] = 'A';
	snprintf(state->ts.ch.file_size, 12, "%011o", state->ts.bytes_archived);
	set_cksum(&state->ts.ch);
	memcpy(&state->inbuf, &state->ts.ch, sizeof(struct tar_header));
	strm->avail_in = sizeof(struct tar_header);
	strm->next_in = state->inbuf;
	state->submit += strm->avail_in;
	return true;
}

static bool compress(struct splitter_state *state, lzma_stream *strm)
{
	lzma_ret ret;
	lzma_action action = LZMA_RUN;
	char fname[1024];
	if (!init_encoder(strm, state->preset))
		return false;

	strm->next_in = NULL;
	strm->avail_in = 0;
	strm->next_out = state->outbuf;
	strm->avail_out = sizeof(state->outbuf);


	state->submit = 0;
	state->current_file_size = 0;
	state->fno = 0;

	snprintf(fname, 1023, state->template, state->fno);
	state->outfile = fopen(fname, "w");

	while (!feof(state->infile)) {
		if (strm->avail_in == 0) {
			if (!get_input(state, strm))
				return false;
		}

		ret = lzma_code(strm, LZMA_RUN);
		if (ret != LZMA_OK) {
			fprintf(stderr, "Error %d while generating compressed data.\n", ret);
			exit(EXIT_FAILURE);
		}
		if (!write_out(state, strm))
			return false;

		if (state->submit + state->current_file_size > state->max_file_size) {
			if (state->submit) {
				if (!flush(state, strm, LZMA_SYNC_FLUSH))
					return false;
				state->submit = 0;
			}
		}

		if (state->submit + state->current_file_size > state->max_file_size) {
			int zero_fill = state->ts.bytes_left;
			memset(state->inbuf, 0, sizeof(state->inbuf));
			while (zero_fill > 0) {
				strm->next_in = state->inbuf;
				strm->avail_in = sizeof(state->inbuf) > zero_fill ? zero_fill : sizeof(state->inbuf);
				zero_fill -= strm->avail_in;
				ret = lzma_code(strm, LZMA_RUN);
				if (ret != LZMA_OK) {
					fprintf(stderr, "Error while zero padding at end of archive\n");
					exit(EXIT_FAILURE);
				}
				if (!write_out(state, strm))
					return false;
			}
			if (!flush(state, strm, LZMA_FINISH))
				return false;
			state->current_file_size = 0;
			fclose(state->outfile);
			state->fno++;
			snprintf(fname, 1023, state->template, state->fno);
			state->outfile = fopen(fname, "w");
			if (!init_encoder(strm, state->preset))
				return false;
			if (state->verbose)
				fprintf(stderr, "Creating file %s\n", fname);
			if (!repeat_header(state, strm))
				return false;
		}

		if (ret != LZMA_OK) {
			if (ret == LZMA_STREAM_END && action == LZMA_FINISH)
				return true;
			if (ret == LZMA_STREAM_END)
				continue;
			fprintf(stderr, "Failure to complete output file.\n");
			return false;
		}
	}
	do {
		ret = lzma_code(strm, LZMA_FINISH);
		if (strm->avail_out == 0 || ret == LZMA_STREAM_END) {
			size_t write_size = sizeof(state->outbuf) - strm->avail_out;

			if (fwrite(state->outbuf, 1, write_size, state->outfile)
					!= write_size) {
				fprintf(stderr, "Write error: %s\n",
						strerror(errno));
				return false;
			}

			strm->next_out = state->outbuf;
			strm->avail_out = sizeof(state->outbuf);
			state->current_file_size += sizeof(state->outbuf);
		}
	} while (ret == LZMA_OK);
	ret = LZMA_OK;
	lzma_end(strm);
	return true;
}


extern int
main(int argc, char **argv)
{
	char *basename = "output";
	int digits = 9;
	int opt;
	bool success;
	struct splitter_state state;

	memset(&state, 0, sizeof(state));

	lzma_stream strm = LZMA_STREAM_INIT;

	state.max_file_size = 1 * 1024 * 1024;
	state.preset = 6;
	state.verbose = false;
	state.extreme = false;
	state.parse_tar = true; //can't currently be disabled
	state.infile = stdin;

	while ((opt = getopt(argc, argv, "p:ets:vb:f:d:")) != -1) {
		switch (opt) {
		case 'v':
			state.verbose = true;
			break;
		case 'e':
			state.extreme = true;
			break;
		case 't':
			state.parse_tar = true;
			break;
		case 'p':
			state.preset = atoi(optarg);
			break;
		case 's':
			state.max_file_size = atoi(optarg) * 1024 * 1024;
			break;
		case 'f':
			state.infile = fopen(optarg, "r");
			if (!state.infile) {
				fprintf(stderr, "Unable to open input file: %s\n", optarg);
				exit(EXIT_FAILURE);
			}
			break;
		case 'b':
			basename = strdup(optarg);
			if (strstr(optarg, "%")) {
				printf("Unreasonable basename (contains a %%): %s\n", basename);
				exit(EXIT_FAILURE);
			}
			break;
		case 'd':
			digits = atoi(optarg);
			break;
		default: /* '?' */
			usage();
			exit(EXIT_FAILURE);
		}
	}
	snprintf(state.template, 1024, "%s%%0%dd.xz", basename, digits);
	if (state.extreme)
		state.preset |= LZMA_PRESET_EXTREME;
	success = compress(&state, &strm);
	if (state.verbose) {
		if (success)
			fprintf(stderr, "Operation completed successfully\n");
		else
			fprintf(stderr, "Operation did not finish successfully\n");
	}
	return success ? EXIT_SUCCESS : EXIT_FAILURE;
}
